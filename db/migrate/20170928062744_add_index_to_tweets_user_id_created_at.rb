class AddIndexToTweetsUserIdCreatedAt < ActiveRecord::Migration[5.1]
  def change
      add_index :tweets, [:user_id, :created_at]
  end
end

class AddTimestampsToRelationships < ActiveRecord::Migration[5.1]
  def change
    change_table :relationships do |t|
    t.timestamps
    end
  end
end
